using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using StudentManagement_Model.Models.Mapping;
using StudentManagement_Model.Models;

namespace StudentManagement_DAL
{
    public partial class TestDB_PrizeContext : DbContext
    {
        static TestDB_PrizeContext()
        {
            Database.SetInitializer<TestDB_PrizeContext>(null);
        }

        public TestDB_PrizeContext()
            : base("Name=TestDB_PrizeContext")
        {
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Student_Course> Student_Course { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CourseMap());
            modelBuilder.Configurations.Add(new StudentMap());
            modelBuilder.Configurations.Add(new Student_CourseMap());
        }
    }
}
