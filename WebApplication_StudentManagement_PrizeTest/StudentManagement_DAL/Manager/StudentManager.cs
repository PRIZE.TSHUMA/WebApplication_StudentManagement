﻿using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace StudentManagement_DAL.Manager
{
    public class StudentManager : IDisposable
    {
        private readonly TestDB_PrizeContext _context = new TestDB_PrizeContext();
        public bool CreateStudent(Student model)
        {
            bool result = false;
            _context.Students.Add(model);
            _context.SaveChanges();
            result = true;
            return result;
        }

        public List<SelectListItem> PopulateStudent()
        {
            List<Student> studentList = GetAllStudent();
            return studentList.Select(x => new SelectListItem()
            {
                Value = x.StudentId.ToString(),
                Text = string.Format("{0} {1} {2}", x.FirstName, x.Surname,x.EmailAddress)
            }).OrderBy(x => x.Text)
            .ToList();
        }

        public List<Student> GetAllStudent()
        {
            return _context.Students.ToList();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _context != null)
                {
                    _context.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}
