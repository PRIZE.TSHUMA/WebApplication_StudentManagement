﻿using StudentManagement_DAL.Constants;
using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement_DAL.Manager
{
    public class StudentCourseManager : IDisposable
    {
        private readonly TestDB_PrizeContext _context = new TestDB_PrizeContext();

        public bool AddStudentCourse(int studentId, int courseId)
        {
            bool result = false;
            Student_Course model = new Student_Course();
            model.StudentId = studentId;
            model.CourseId = courseId;
            _context.Student_Course.Add(model);
            _context.SaveChanges();
            result = true;
            return result;
        }

        public List<Student_Course> GetAllStudent_Course()
        {
            return _context.Student_Course.ToList();
        }

        public bool IsStudentRegisteredMoreThanNCourse(int studentId)
        {
            bool result = true;
            var studentCourseList = _context.Student_Course.Where(x => x.StudentId == studentId).ToList();
            if(studentCourseList == null || !studentCourseList.Any() || studentCourseList.Count < StudentCourseConstant.MAXIMUMREGISTRATIONCOURSE)
            {
                result = false;
            }
            return result;
        }

        public bool IsCourseAlreadyAssignedStudent(int studentId,int courseId)
        {
            bool result = true;
            var studentCourse = _context.Student_Course.Where(x => x.StudentId == studentId && x.CourseId == courseId).FirstOrDefault();
            if (studentCourse == null)
            {
                result = false;
            }
            return result;
        }

        #region IDisposable Support
        private bool disposedValue = false;

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _context != null)
                {
                    _context.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}
