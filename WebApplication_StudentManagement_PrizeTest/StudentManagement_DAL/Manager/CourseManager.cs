﻿using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace StudentManagement_DAL.Manager
{
    public class CourseManager : IDisposable
    {
    private readonly TestDB_PrizeContext _context = new TestDB_PrizeContext();

        public bool CreateCourse(Course model)
        {
            bool result = false;
            _context.Courses.Add(model);
            _context.SaveChanges();
            result = true;
            return result;
        }

        public List<SelectListItem> PopulateCourse()
        {
            List<Course> courseList = GetAllCourse();
            return courseList.Select(x => new SelectListItem()
            {
                Value = x.CourseId.ToString(),
                Text = x.CourseName
            }).OrderBy(x => x.Text)
            .ToList();
        }

        public List<Course> GetAllCourse()
        {
            return _context.Courses.ToList();
        }

        #region IDisposable Support
        private bool disposedValue = false;

    private void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing && _context != null)
            {
                _context.Dispose();
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    #endregion IDisposable Support
    }
}
