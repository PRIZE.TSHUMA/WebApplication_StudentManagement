using System;
using System.Collections.Generic;

namespace StudentManagement_Model.Models
{
    public partial class Student
    {
        public Student()
        {
            this.Student_Course = new List<Student_Course>();
        }

        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string IDNumber { get; set; }
        public virtual ICollection<Student_Course> Student_Course { get; set; }
    }
}
