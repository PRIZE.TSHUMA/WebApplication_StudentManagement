using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace StudentManagement_Model.Models.Mapping
{
    public class CourseMap : EntityTypeConfiguration<Course>
    {
        public CourseMap()
        {
            this.HasKey(t => t.CourseId);

            // Properties
            this.Property(t => t.CourseName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Course");
            this.Property(t => t.CourseId).HasColumnName("CourseId");
            this.Property(t => t.CourseName).HasColumnName("CourseName");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
        }
    }
}
