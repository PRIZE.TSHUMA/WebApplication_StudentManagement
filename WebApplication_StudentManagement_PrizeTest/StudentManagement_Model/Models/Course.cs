using System;
using System.Collections.Generic;

namespace StudentManagement_Model.Models
{
    public partial class Course
    {
        public Course()
        {
            this.Student_Course = new List<Student_Course>();
        }

        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public virtual ICollection<Student_Course> Student_Course { get; set; }
    }
}
