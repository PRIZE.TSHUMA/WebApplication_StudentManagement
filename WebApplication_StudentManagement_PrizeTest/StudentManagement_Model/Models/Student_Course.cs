using System;
using System.Collections.Generic;

namespace StudentManagement_Model.Models
{
    public partial class Student_Course
    {
        public int StudentCourseId { get; set; }
        public int StudentId { get; set; }
        public int CourseId { get; set; }
        public virtual Course Course { get; set; }
        public virtual Student Student { get; set; }
    }
}
