﻿using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication_StudentManagement_PrizeTest.Models;

namespace WebApplication_StudentManagement_PrizeTest.App_Start
{
internal class MapProfile : AutoMapper.Profile
    {
        protected  void Configure()
        {
            CreateMap<Student_Course, Student_Course_Info_ViewModel>();
            CreateMap<Student_Course_Info_ViewModel, Student_Course>();
        }
    }
}
