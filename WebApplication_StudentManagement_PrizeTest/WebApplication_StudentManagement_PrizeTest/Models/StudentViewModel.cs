﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication_StudentManagement_PrizeTest.Models
{
    public class StudentViewModel
    {
        public int StudentId { get; set; }
        [Display(Name = "First Name :")]
        [Required(ErrorMessage = "First Name is required!")]
        public string FirstName { get; set; }
        [Display(Name = "Surname :")]
        [Required(ErrorMessage = "Surname is required!")]
        public string Surname { get; set; }
        [Display(Name = "Email Address :")]
        [RegularExpression(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?")]
        public string EmailAddress { get; set; }
        [Display(Name = "ID Number :")]
        public string IDNumber { get; set; }
    }
}
