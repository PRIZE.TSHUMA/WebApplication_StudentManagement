﻿using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebApplication_StudentManagement_PrizeTest.Models
{
    public class StudentCourseViewModel
    {
        public int StudentId { get; set; }
        public List<SelectListItem> Students { get; set; }

        public int CourseId { get; set; }
        public List<SelectListItem> Courses { get; set; }
    }
}
