﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication_StudentManagement_PrizeTest.Models
{
    public class CourseViewModel : IValidatableObject
    {
        public int CourseId { get; set; }
        [Required(ErrorMessage = "Course Name is required!")]
        public string CourseName { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Start Date [dd-MMM-yyyy] :")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> StartDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "End Date [dd-MMM-yyyy] :")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy}")]
        public Nullable<System.DateTime> EndDate { get; set; }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            if (EndDate < StartDate)
            {
                yield return new ValidationResult("EndDate must be greater than StartDate");
            }
        }
    }
}
