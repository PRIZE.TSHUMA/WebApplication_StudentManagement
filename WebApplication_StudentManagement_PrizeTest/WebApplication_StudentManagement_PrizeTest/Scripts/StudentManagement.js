﻿function AjaxOnSuccess(data, callbackFunction) {

    if (!data.success && data.errors) {
        OnError(data.errors);
    } else {
        if (data != null) {
            if (data.NavigateTo) NavigateTo(data.NavigateTo);
            if (data.InformationMessage) showInformation(data.InformationMessage, data.duration);
            if (data.LoadingMessage) showLoadingMessage(data.LoadingMessage, data.completedEvent, data.completedNavigateTo);
        }
        if (typeof callbackFunction == 'function') {
            callbackFunction.call(this);
        }
    }
}

function showInformation(message, messageDisplayDuration) {
    var duration = 5; // in seconds
    if (messageDisplayDuration) { duration = messageDisplayDuration }
    $('#information').html(message).addClass('alert alert-success').attr('role', 'alert');
    $('#information').slideDown();
    $("#information").on("click", function () {
        $("#information").off("click");
        $('#information').slideUp();
    });
    //Only show info div for 5 sec
    if ($('#timer') === undefined || $('#timer').length == 0) {
        setTimeout(function () { $('#information').slideUp(); }, duration * 1000);
        $('scrollToTopButton li a').click();
    }
}

function OnError(errors) {
    if (document.getElementById("windowErrors") !== null) {
        showWindowErrors(errors);
        return;
    }

    var errorData = errors;
    var errorMessages = [];
    $.each(errors, function (key, value) {
        errorMessages.push('<li class="error">' + value + '</li>');
    });

    $('#errors .errorContent').html(errorMessages.join("<br />"));
    $('#errors').fadeIn();
    $('ul.alert').fadeIn();

    $("#errors").on("click", function () {
        $("#errors").off("click");
        $('ul.alert').slideUp();
        $('#errors .errorContent').html("");
    });

    setTimeout(function () {
        $('ul.alert').fadeOut('slow');
        $('#errors .errorContent').html("");
    }, 200000);

    $('form').find('[data-enable-before-disable="true"]').removeAttr('disabled');
}

function InitialiseDatePicker() {

    $('input[type="text"].datefield').not('[readonly=readonly]').datepicker({ dateFormat: 'dd M yy' }).each(function () {
        if ($(this).attr('datamindateValue')) {
            $(this).datepicker("option", "minDate", $(this).attr('datamindateValue'));
        }

        if ($(this).attr('datamaxdateValue')) {
            $(this).datepicker("option", "maxDate", $(this).attr('datamaxdateValue'));
        }
    });
}