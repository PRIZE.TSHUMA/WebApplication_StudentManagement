﻿using AutoMapper;
using StudentManagement_DAL.Constants;
using StudentManagement_DAL.Manager;
using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication_StudentManagement_PrizeTest.Models;

namespace WebApplication_StudentManagement_PrizeTest.Controllers
{
    public class StudentCourseController : BaseController
    {
        // GET: StudentCourse
        public ActionResult Index()
        {
            List<Student_Course_Info_ViewModel> model = new List<Student_Course_Info_ViewModel>(); 
            using (StudentCourseManager stuCMnger = new StudentCourseManager())
            {
                var modelStudentCourseDB = stuCMnger.GetAllStudent_Course();
                Mapper.Configuration.CreateMapper();
                model = Mapper.Map<List<Student_Course>, List<Student_Course_Info_ViewModel>>(modelStudentCourseDB);
            }
            return PartialView(model);
        }

        public ActionResult AddStudentCourse(int studentId, int courseId)
        {
            bool result = false;
            using (StudentCourseManager stuCourseMngr = new StudentCourseManager())
            {
                if (stuCourseMngr.IsCourseAlreadyAssignedStudent(studentId, courseId))
                {
                    return base.RaiseError("Each course should not be assigned more than once to a student");
                }

                if (stuCourseMngr.IsStudentRegisteredMoreThanNCourse(studentId))
                {
                    return base.RaiseError(string.Format("A student should not be assigned more than {0} courses", StudentCourseConstant.MAXIMUMREGISTRATIONCOURSE));
                }

                //if we got here lets insert
                result = stuCourseMngr.AddStudentCourse(studentId, courseId);
            }

            if (result)
            {
                return base.RaiseSuccess("Updated Successfully");
            }
            else
            {
                return base.RaiseError("Update Failed");
            }
        }
    }
}