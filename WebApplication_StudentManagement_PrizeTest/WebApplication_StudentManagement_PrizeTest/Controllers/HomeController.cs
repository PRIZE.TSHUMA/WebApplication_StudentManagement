﻿using StudentManagement_DAL.Constants;
using StudentManagement_DAL.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication_StudentManagement_PrizeTest.Models;

namespace WebApplication_StudentManagement_PrizeTest.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            StudentCourseViewModel model = new StudentCourseViewModel();
            using (StudentManager stuManager = new StudentManager())
            {
                model.Students = stuManager.PopulateStudent();
            }

            using (CourseManager courseManager = new CourseManager())
            {
                model.Courses = courseManager.PopulateCourse();
            }

            return View(model);
        }        
    }
}