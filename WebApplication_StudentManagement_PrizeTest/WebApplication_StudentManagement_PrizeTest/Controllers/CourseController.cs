﻿using AutoMapper;
using StudentManagement_DAL.Manager;
using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication_StudentManagement_PrizeTest.Models;

namespace WebApplication_StudentManagement_PrizeTest.Controllers
{
    public class CourseController : BaseController
    {
        // GET: Course
        public ActionResult Index()
        {
            List<CourseViewModel> courseList = new List<CourseViewModel>();
            using (CourseManager courseMnger = new CourseManager())
            {
                var modelCourseDB = courseMnger.GetAllCourse();
                Mapper.Configuration.CreateMapper();
                courseList = Mapper.Map<List<Course>, List<CourseViewModel>>(modelCourseDB);
            }
            return View(courseList);
        }

        // GET: Course/Create
        public ActionResult Create()
        {
            CourseViewModel model = new CourseViewModel();
            return PartialView(model);
        }

        // POST: Course/Create
        [HttpPost]
        public ActionResult Create(CourseViewModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                using (CourseManager courseMngr = new CourseManager())
                {
                    Course updateDBModel = new Course();
                    updateDBModel.CourseName = model.CourseName;
                    updateDBModel.StartDate = model.StartDate;
                    updateDBModel.EndDate = model.EndDate;
                    result = courseMngr.CreateCourse(updateDBModel);
                }
            }
            else
            {
                return base.RaiseError("Invalid course information.");
            }

            if (result)
            {
                return base.RaiseSuccess("Updated Successfully");
            }
            else
            {
                return base.RaiseError("Update Failed");
            }
        }
    }
}
