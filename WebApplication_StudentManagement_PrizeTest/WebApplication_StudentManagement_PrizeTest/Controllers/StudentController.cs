﻿using AutoMapper;
using StudentManagement_DAL.Manager;
using StudentManagement_Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication_StudentManagement_PrizeTest.Models;

namespace WebApplication_StudentManagement_PrizeTest.Controllers
{
    public class StudentController : BaseController
    {
        // GET: Student
        public ActionResult Index()
        {
            List<StudentViewModel> model = new List<StudentViewModel>();
            using (StudentManager stuMnger = new StudentManager())
            {
                var modelStudentDB = stuMnger.GetAllStudent();
                Mapper.Configuration.CreateMapper();
                model = Mapper.Map<List<Student>, List<StudentViewModel>>(modelStudentDB);
            }

            return View(model);
        }
        
        // GET: Student/Create
        public ActionResult Create()
        {
            StudentViewModel model = new StudentViewModel();
            return PartialView(model);
        }

        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(StudentViewModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                using (StudentManager studentMngr = new StudentManager())
                {
                    Student updateDBModel = new Student();
                    updateDBModel.FirstName = model.FirstName;
                    updateDBModel.Surname = model.Surname;
                    updateDBModel.EmailAddress = model.EmailAddress;
                    updateDBModel.IDNumber = model.IDNumber;
                    result = studentMngr.CreateStudent(updateDBModel);
                }
            }
            else
            {
                return base.RaiseError("Invalid student information.");
            }

            if (result)
            {
                return base.RaiseSuccess("Updated Successfully");
            }
            else
            {
                return base.RaiseError("Update Failed");
            }
        }
    
    }
}
